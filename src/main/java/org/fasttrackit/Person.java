package org.fasttrackit;

public class Person {
    private final String name;
    private final String firstName;
    private final String gender;
    private final String id;
    private final String pinCode;
    private final String completedSteps;

    public Person(String name, String firstName, String gender, String id, String pinCode, String completedSteps) {
        this.name = name;
        this.firstName = firstName;
        this.gender = gender;
        this.id = id;
        this.completedSteps = completedSteps;
        this.pinCode= pinCode;
    }
    public  String getPinCode() {
        return pinCode;
    }
    public String getFullName() {
        return name + " " + firstName;
    }
    public String getCompletedSteps() {
        return completedSteps;
    }
}
