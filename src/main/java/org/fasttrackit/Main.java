package org.fasttrackit;

import org.fasttrackit.activities.*;

import java.util.Scanner;

public class Main {

    public static final String APP_NAME = "Smart Fitness EU";
    public static final String APP_LABEL = "- = " + APP_NAME + " = -";
    private static final Display display = new Display();
    public static String firstName;
    private static Scanner in;

    public static void main(String[] args) {
        Person user = greetUser();
        in = authenticateUser(user);
        if (in == null)
            return;
        display.showActivityMenu();
        String userAnswer = interrogateUser(in);
        chooseActivity(userAnswer);
        display.showAdditionalActivityMenu();
        userAnswer = interrogateUser(in);

        while (userAnswer.equalsIgnoreCase("yes")) {
            display.showActivityMenu();
            userAnswer = interrogateUser(in);
            chooseActivity(userAnswer);
            display.showAdditionalActivityMenu();
            userAnswer = interrogateUser(in);
            if (userAnswer.equalsIgnoreCase("no")) {
                break;
            }
        }
        display.goodbyeMessage();
    }

    private static String interrogateUser(Scanner scanner) {
        return scanner.nextLine();
    }

    private static void chooseActivity(String activity) {
        switch (activity) {
            case "1": {
                SwimmingActivity swimming = new SwimmingActivity("light");
                display.printChosenActivity(swimming);
                display.printMessage(swimming.askAboutNumberOfPools());
                String userInput = interrogateUser(in);
                int noOfPools = Integer.parseInt(userInput);
                swimming.withNoOfPools(noOfPools);
                display.printMessage(swimming.provideSwimingStatistics());
                break;
            }
            case "2": {
                RunningActivity running = new RunningActivity("light");
                display.printChosenActivity(running);
                display.printMessage(running.askAboutNumberOfKm());
                String userInput = interrogateUser(in);
                int noOfKm = Integer.parseInt(userInput);
                running.withNoOfKm(noOfKm);
                display.printMessage(running.provideRunningStatistics());
                break;
            }
            case "3": {
                DancingActivity dancing = new DancingActivity("light");
                display.printChosenActivity(dancing);
                display.printMessage(dancing.askAboutNoDancingMinutes());
                String userInput = interrogateUser(in);
                int noOfDancingMinutes = Integer.parseInt(userInput);
                dancing.withNoOfDancingMinutes(noOfDancingMinutes);
                display.printMessage(dancing.provideDancingStatistics());
                break;
            }
            case "5": {
                FitnessActivity fitness = new FitnessActivity("light");
                display.printChosenActivity(fitness);
                display.printMessage(fitness.askAboutNoOfFitnessMinutes());
                String userInput = interrogateUser(in);
                int noOfFitnessMinutes = Integer.parseInt(userInput);
                fitness.withNoOfFitnessMinutes(noOfFitnessMinutes);
                display.printMessage(fitness.provideFitnessStatistics());
                break;
            }
            case "6": {
                YogaActivity yoga = new YogaActivity("light");
                display.printChosenActivity(yoga);
                display.printMessage(yoga.askAboutNoOfYogaMines());
                String userInput = interrogateUser(in);
                int noOfYogaMinutes = Integer.parseInt(userInput);
                yoga.withNoOfYogaMinutes(noOfYogaMinutes);
                display.printMessage(yoga.provideYogaStatistics());
                break;
            }
            case "7": {
                Scanner scanner = new Scanner(System.in);
                display.askUserBodyInformation();
                display.askUserWeightInformation();
                double weight = scanner.nextDouble();
                display.askUserHeightInformation();
                double height = scanner.nextDouble();
                double bmi = weight / (height * height);
                System.out.println("Your BMI is: " + bmi + " !");
                if ( bmi <= 18.5) {
                    System.out.println("Underweight.");
                } else if ( bmi <=25 ) {
                    System.out.println("Normal weight.");
                } else if ( bmi <=30 ) {
                    System.out.println("Overweight.");
                }
                else {
                    System.out.println("Obesity.");
                }
                break;
            }
            case "4": {
                display.showCyclingActivityMenu();
                String cyclingActivity =interrogateUser(in);
                switch (cyclingActivity) {
                    case "1": {
                        System.out.println("How many minutes did you practice Outdoor Cycling today?");
                        int noOfOutdoorCyclingMinutes = 0;
                        int calculateOutdoorCyclingBurntCalories = 0;
                        final int burntCaloriesPerKmOutdoorCycling = 20;
                        Scanner scanner = new Scanner(System.in);
                        noOfOutdoorCyclingMinutes = Integer.parseInt(scanner.nextLine());
                        calculateOutdoorCyclingBurntCalories = noOfOutdoorCyclingMinutes * burntCaloriesPerKmOutdoorCycling;
                        System.out.println("You have practiced Outdoor Cycling for " + noOfOutdoorCyclingMinutes + " minutes, thus you burned " + calculateOutdoorCyclingBurntCalories + " calories.");
                        break;
                    }
                    case "2": {
                        System.out.println("How many minutes did you practice Indoor Cycling today?");
                        int noOfIndoorCyclingMinutes = 0;
                        int calculateIndoorCyclingBurntCalories = 0;
                        final int burntCaloriesPerKmIndoorCycling = 15;
                        Scanner scanner = new Scanner(System.in);
                        noOfIndoorCyclingMinutes = Integer.parseInt(scanner.nextLine());
                        calculateIndoorCyclingBurntCalories = noOfIndoorCyclingMinutes * burntCaloriesPerKmIndoorCycling;
                        System.out.println("You have practiced Indoor Cycling for " + noOfIndoorCyclingMinutes + " minutes, thus you burned " + calculateIndoorCyclingBurntCalories + " calories.");
                        break;
                    }
                    case "3": {
                        System.out.println("How many minutes did you practice Spinning Cycling today?");
                        int noOfSpinningCyclingMinutes = 0;
                        int calculateSpinningCyclingBurntCalories = 0;
                        final int burntCaloriesPerKmSpinningCycling = 25;
                        Scanner scanner = new Scanner(System.in);
                        noOfSpinningCyclingMinutes = Integer.parseInt(scanner.nextLine());
                        calculateSpinningCyclingBurntCalories = noOfSpinningCyclingMinutes * burntCaloriesPerKmSpinningCycling;
                        System.out.println("You have practiced Spinning Cycling for " + noOfSpinningCyclingMinutes + " minutes, thus you burned " + calculateSpinningCyclingBurntCalories + " calories.");
                        break;
                    }
                    case "4": {
                        System.out.println("How many minutes did you practice Soul Cycling today?");
                        int noOfSoulCyclingMinutes = 0;
                        int calculateSoulCyclingBurntCalories = 0;
                        final int burntCaloriesPerKmSoulCycling = 30;
                        Scanner scanner = new Scanner(System.in);
                        noOfSoulCyclingMinutes = Integer.parseInt(scanner.nextLine());
                        calculateSoulCyclingBurntCalories = noOfSoulCyclingMinutes * burntCaloriesPerKmSoulCycling;
                        System.out.println("You have practiced Soul Cycling for " + noOfSoulCyclingMinutes + " minutes, thus you burned " + calculateSoulCyclingBurntCalories + " calories.");
                        break;
                    }
                }
            }
        }
    }

    private static Scanner authenticateUser(Person user) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please Enter Your PIN code:");
        String pinCode = interrogateUser(scanner);
        if (!user.getPinCode().equals(pinCode)) {
            System.out.println("Wrong PIN!");
            return null;
        }
        System.out.println("Successfully logged in.");
        return scanner;
    }

    private static Person greetUser() {
        display.printAppName();
        Scanner scanner = new Scanner(System.in);
        display.askUserForName();
        String name = interrogateUser(scanner);
        Person user = new Person(name, "Andreica", "male", "1820203125789", "9", "9800");
        display.greetKnownUser(user);
        return user;
    }
}