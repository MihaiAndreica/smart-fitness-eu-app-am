package org.fasttrackit.activities;

public class FitnessActivity extends Activity {
    private final static String FITNESS = "Fitness";
    private final static int BURNT_CALORIES_PER_FITNESS_MINUTE = 7;
    private int noOfFitnessMinutesMade = 0;

    public FitnessActivity(String intensity) {
        super(intensity, FITNESS);
    }

    public String askAboutNoOfFitnessMinutes() {
        return "How many minutes did you make fitness today?";
    }

    public void withNoOfFitnessMinutes(int noOfFitnessMinutes) {
        this.noOfFitnessMinutesMade = noOfFitnessMinutes;
    }

    public String provideFitnessStatistics() {
        return "You have done fitness for " + noOfFitnessMinutesMade + " minutes, thus you burned " + calculateFitnessBurntCalories() + " calories.";
    }

    public int calculateFitnessBurntCalories() {
        int calories = this.noOfFitnessMinutesMade * BURNT_CALORIES_PER_FITNESS_MINUTE;
        return calories;
    }
}

