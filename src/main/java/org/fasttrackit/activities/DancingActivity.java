package org.fasttrackit.activities;

public class DancingActivity extends Activity {
    private final static String DANCING = "Dancing";
    private final int BURNT_CALORIE_PER_DANCING_MINUTE = 5;
    private int noOfMinutesDanced = 0;

    public DancingActivity(String intensity) {
        super(intensity, DANCING);
    }

    public String askAboutNoDancingMinutes() {
        return "How many minutes did you dance today?";
    }

    public void withNoOfDancingMinutes(int noOfDancingMinutes) {
        this.noOfMinutesDanced = noOfDancingMinutes;
    }

    public String provideDancingStatistics() {
        return "You have been dancing for " + noOfMinutesDanced + " minutes, while you burned " + calculateDancingBurntCalories() + " calories.";
    }

    public int calculateDancingBurntCalories() {
        int calories = this.noOfMinutesDanced * BURNT_CALORIE_PER_DANCING_MINUTE;
        return calories;
    }
}
