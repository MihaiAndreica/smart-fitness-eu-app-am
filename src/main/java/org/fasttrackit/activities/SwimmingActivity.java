package org.fasttrackit.activities;

public class SwimmingActivity extends Activity {
    private static final String SWIMMING = "Swimming";
    private static final int POOL_LENGHT = 50;
    private static final int BURNT_CALORIES_PER_POOL = 200;
    private int noOfPools = 0;
    public SwimmingActivity(String intensity) {
        super(intensity, SWIMMING);
    }
    public String askAboutNumberOfPools(){
        return "How many swimming pools did you complete today?";
    }
    public String provideSwimingStatistics(){
        return "You have burnt " + getBurnedCallories() + " calories in " + getSwimingDistance() + " meters swim";
    }

    public void withNoOfPools(int noOfPools) {
        this.noOfPools = noOfPools;
    }
    public int getBurnedCallories(){
        int burntCalories = BURNT_CALORIES_PER_POOL * this.noOfPools;
        return burntCalories;
    }
    public int getSwimingDistance(){
        int swimingDistance = POOL_LENGHT * this.noOfPools;
        return swimingDistance;
    }
}
