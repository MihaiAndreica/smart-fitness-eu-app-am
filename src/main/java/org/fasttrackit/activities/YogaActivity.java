package org.fasttrackit.activities;

public class YogaActivity extends Activity {
    private final static String YOGA = "Yoga";
    private final static int BURNT_CALORIES_PER_YOGA_MINUTE = 3;
    private int noOfYogaMinutesMade = 0;

    public YogaActivity(String intensity) {
        super(intensity, YOGA);
    }

    public String askAboutNoOfYogaMines() {
        return "How many minutes did you make Yoga today?";
    }

    public void withNoOfYogaMinutes(int noOfYogaMinutes) {
        this.noOfYogaMinutesMade = noOfYogaMinutes;
    }

    public String provideYogaStatistics() {
        return "You have done fitness for " + noOfYogaMinutesMade + " minutes, thus you burned " + calculateYogaBurntCalories() + " calories.";
    }

    public int calculateYogaBurntCalories() {
        int calories = this.noOfYogaMinutesMade * BURNT_CALORIES_PER_YOGA_MINUTE;
        return calories;
    }
}