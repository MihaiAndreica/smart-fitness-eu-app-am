package org.fasttrackit.activities;

public class CyclingActivity extends Activity{
    private final String intensity;
    private final String cyclingType;

    public CyclingActivity(String intensity, String cyclingType) {
        super("light",cyclingType);
        this.intensity = intensity;
        this.cyclingType = cyclingType;
    }
    public String getCyclingType() {
        return cyclingType;
    }
}
