package org.fasttrackit.activities;

public class Activity {
    public final String intensity;
    public final String name;
    public Activity(String intensity, String name) {
        this.intensity = intensity;
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public String getIntensity() {
        return intensity;
    }
}