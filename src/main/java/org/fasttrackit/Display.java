package org.fasttrackit;

import org.fasttrackit.activities.Activity;
import org.fasttrackit.activities.CyclingActivity;

import static org.fasttrackit.Main.*;

public class Display {
    public void printAppName() {
        System.out.println(APP_LABEL);
    }
    public void askUserForName() {
        System.out.println("What is your name?");
    }
    public void greetKnownUser(Person user) {
        System.out.println("Welcome to the " + APP_NAME + " " + user.getFullName() + " !");
    }
    public void askUserBodyInformation() {
        System.out.println("In order to provide your BMI -Body Mass Index we need some info from you :");
    }
    public void askUserWeightInformation() {
        System.out.println("Please enter your weight in kilograms: ");
    }
    public void askUserHeightInformation() {
        System.out.println("Please enter your height in meters: ");
    }

    public void showActivityMenu() {
//        System.out.println("Congrats! You have reached " + NO_OF_STEPS + " steps.");
        System.out.println("What would you like to do/train today?");
        System.out.println("1. Swimming");
        System.out.println("2. Running");
        System.out.println("3. Dancing");
        System.out.println("4. Cycling");
        System.out.println("5. Fitness");
        System.out.println("6. Yoga");
        System.out.println("7. Or you would like to find out you BMI -Body Mass Index?");
    }
    public void showCyclingActivityMenu() {
        System.out.println("You have chosen cycling from the Fitness menu.");
        System.out.println("What type of Cycling would you like to do?");
        System.out.println("1. Outdoor Cycling");
        System.out.println("2. Indoor Cycling");
        System.out.println("3. Spinning Cycling");
        System.out.println("4. Soul Cycling");
    }
    public void showAdditionalActivityMenu(){
        System.out.println("Would you like to add additional activities?");
        System.out.println("Yes");
        System.out.println("No");
    }
    public void printChosenActivity(Activity activity) {
        System.out.println("You have chosen " + activity.getName() + " from the Fitness Menu.");
    }
    public void printMessage(String msg) {
        System.out.println(msg);
    }
    public void showAdditionalCyclingActivityMenu() {
        System.out.println("Would you like to add additional CyclingActivities?");
        System.out.println("Yes");
        System.out.println("No");
    }
    public void printChosenCyclingActivity(CyclingActivity cyclingActivity) {
        System.out.println("You have chosen " + cyclingActivity.getCyclingType() + " from the Cycling Menu.");
    }
    public void goodbyeMessage(){
        System.out.println("See ya tomorrow!");
    }
}
